import Foundation

public final class WhereAmIRunning {
    class var isRunningInTestFlightEnvironment: Bool{
        guard isSimulator == false else { return false }
        
        return (isAppStoreReceiptSandbox && !hasEmbeddedMobileProvision)
    }
    
    class var isRunningInAppStoreEnvironment: Bool {
        guard isSimulator == false else { return false }
        
        return (isAppStoreReceiptSandbox || hasEmbeddedMobileProvision) == false
    }
}

private extension WhereAmIRunning {
    class private var hasEmbeddedMobileProvision: Bool{
        guard let _ = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") else { return false }
        
        return true
    }
    
    class private var isAppStoreReceiptSandbox: Bool {
        guard isSimulator == false else { return false }
        guard let appStoreReceiptURL = Bundle.main.appStoreReceiptURL, appStoreReceiptURL.lastPathComponent == "sandboxReceipt" else { return false }
        
        return true
    }
    
    class private var isSimulator: Bool {
        #if arch(i386) || arch(x86_64)
        return true
        #else
        return false
        #endif
    }
}
