import UIKit


public extension UIViewController {
    enum SpinnerViewTag: Int {
        case general = -9000003
    }
    
    var isVisible: Bool { return self.isViewLoaded && self.view.window != nil }
    
    func startActivityIndicator(tag: SpinnerViewTag = .general) {
        let startActivityIndicatorBlock = {
            if let view = self.view.subviews.filter({ $0.tag == tag.rawValue }).first {
                view.removeFromSuperview()
            }
            
            let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
//            activityIndicator.color = Color.accent.uiColor
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.accessibilityElementsHidden = true

            let overlayView = UIView(frame: .zero)
            overlayView.translatesAutoresizingMaskIntoConstraints = false
            overlayView.backgroundColor = .black
            overlayView.alpha = 0.85
            overlayView.cornerRadius = 6
            overlayView.tag = tag.rawValue
            overlayView.addSubview(activityIndicator)
            overlayView.accessibilityElementsHidden = true

            self.view.addSubview(overlayView)

            let horizontal = activityIndicator.centerXAnchor.constraint(equalTo: overlayView.centerXAnchor)
            let vertical = activityIndicator.centerYAnchor.constraint(equalTo: overlayView.centerYAnchor)
            NSLayoutConstraint.activate([horizontal, vertical])

            let horizontalConstraint = overlayView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            let verticalConstraint = overlayView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
            let height = overlayView.heightAnchor.constraint(equalToConstant: 80)
            let width = overlayView.widthAnchor.constraint(equalToConstant: 80)
            NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, height, width])

           self.view.layoutIfNeeded()

            self.view.isUserInteractionEnabled = false
        }
        
        if Thread.isMainThread {
            startActivityIndicatorBlock()
        } else {
            DispatchQueue.main.async {
                startActivityIndicatorBlock()
            }
        }
    }
    
    func stopActivityIndicator(tag: SpinnerViewTag = .general) {
        let stopActivityIndicatorBlock = {
            if let view = self.view.subviews.filter({ $0.tag == tag.rawValue }).first {
                view.removeFromSuperview()
            }

            self.view.isUserInteractionEnabled = true
        }
        
        if Thread.isMainThread {
            stopActivityIndicatorBlock()
        } else {
            DispatchQueue.main.async {
                stopActivityIndicatorBlock()
            }
        }
    }
}
