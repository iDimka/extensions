import UIKit

public protocol Bluring {
    func addBlur(_ alpha: CGFloat)
}

public extension Bluring where Self: UIView {
    func addBlur(_ alpha: CGFloat = 0.5) {
        // create effect
        let effect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: effect)

        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha

        self.addSubview(effectView)
    }
}

// Conformance
extension UIView: Bluring {}

public extension Date {
    
    func plus(days: Int) -> Date? {
        var dateComponent = DateComponents()
        dateComponent.day = days
        
        return  Calendar.current.date(byAdding: dateComponent, to: self)
    }
}

public extension UIViewController {
    @IBAction func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}

public extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

public extension UIImage {
    func applyBlurEffect(radius: Double = 10) -> UIImage? {
        let context = CIContext(options: nil)
        let imageToBlur = CIImage(image: self)
        let clampFilter = CIFilter(name: "CIAffineClamp")!
        clampFilter.setDefaults()
        clampFilter.setValue(imageToBlur, forKey: kCIInputImageKey)

        //The CIAffineClamp filter is setting your extent as infinite, which then confounds your context. Try saving off the pre-clamp extent CGRect, and then supplying that to the context initializer.
        let inputImageExtent = imageToBlur!.extent

        guard let currentFilter = CIFilter(name: "CIGaussianBlur") else { return nil }
        currentFilter.setValue(clampFilter.outputImage, forKey: kCIInputImageKey)
        currentFilter.setValue(radius, forKey: "inputRadius")
        guard let output = currentFilter.outputImage, let cgimg = context.createCGImage(output, from: inputImageExtent) else { return nil }
        
        return UIImage(cgImage: cgimg)
    }
    
    func imageWithColor(color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        guard let context = UIGraphicsGetCurrentContext(), let cgImage = cgImage else { return  nil }
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: cgImage)
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
